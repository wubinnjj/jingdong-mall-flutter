import 'package:flutter/material.dart';
import '../../config/Config.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:card_swiper/card_swiper.dart';
import 'package:dio/dio.dart';
import '../../model/FocusModel.dart';
import '../../model/ProductModel.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with AutomaticKeepAliveClientMixin {
  //bannar数据
  List _focusData = [];
  //猜你喜欢数据
  List _hotProuctList = [];
  //热门推荐数据
  List _bestProductList = [];

  @override
  void initState() {
    super.initState();
    // _getFocusData();
    // _getBestProductData();
    // _getLikeProductData();
  }

// 获取轮播图数据
  _getFocusData() async {
    var api = '${Config.domain}/api/focus';

    var result = await Dio().get(api);

    var focusList = FocusModel(result: result.data);

    setState(() {
      _focusData = focusList.result!;
    });
  }

// 获取猜你喜欢数据
  _getLikeProductData() async {
    var api = '${Config.domain}api/plist?is_hot=1';
    var result = await Dio().get(api);
    var hotProductList = ProductModel.fromJson(result.data);
    setState(() {
      this._hotProuctList = hotProductList.result!;
    });
  }

// 获取热门推荐数据
  _getBestProductData() async {
    var api = '${Config.domain}api/plist?is_hot=1';
    var result = await Dio().get(api);
    var bestProductList = ProductModel.fromJson(result.data);
    setState(() {
      this._bestProductList = bestProductList.result!;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        _swiperWidget(),
        SizedBox(height: 20.h),
        _titleWidget("猜你喜欢"),
        _likeProductListWidget(),
        SizedBox(height: 20.h),
        _titleWidget("热门推荐"),
        Container(
          // decoration: BoxDecoration(color: Colors.red),
          margin: EdgeInsets.all(10),
          child: Wrap(
            children: [
              _hotProductItemWidget(),
              _hotProductItemWidget(),
              _hotProductItemWidget(),
              _hotProductItemWidget(),
              _hotProductItemWidget(),
              _hotProductItemWidget()
            ],
            runSpacing: 10,
            spacing: 10,
          ),
        )
      ],
    );
  }

  // //热门推荐
  // Widget _recProcuctListWidget() {
  //   var itemWidth = (ScreenUtil().screenWidth - 30) * 0.5;

  //   return Container(
  //     // decoration: BoxDecoration(color: Colors.red),
  //     margin: EdgeInsets.all(10),
  //     child: Wrap(
  //       children: _bestProductList.map((e) {
  //         //图片
  //         String sPic = e.sPic;
  //         sPic = Config.domain + sPic.replaceAll('\\', '/');

  //         return Container(
  //           padding: EdgeInsets.all(10),
  //           width: itemWidth,
  //           decoration: BoxDecoration(
  //               border: Border.all(
  //                   color: Color.fromRGBO(233, 233, 233, 0.9), width: 1)),
  //           child: Column(
  //             children: [
  //               Container(
  //                 width: double.infinity,
  //                 child: AspectRatio(
  //                     aspectRatio: 1 / 1,
  //                     child: Image.network(
  //                       '${sPic}',
  //                       fit: BoxFit.cover,
  //                     )),
  //               ),
  //               Padding(
  //                 padding: EdgeInsets.only(top: 20.h),
  //                 child: Text(
  //                   '${e.title}',
  //                   maxLines: 2,
  //                   overflow: TextOverflow.ellipsis,
  //                   style: TextStyle(color: Colors.black54),
  //                 ),
  //               ),
  //               Padding(
  //                 padding: EdgeInsets.only(top: 20.h),
  //                 child: Stack(children: [
  //                   Align(
  //                     alignment: Alignment.centerLeft,
  //                     child: Text(
  //                       "${e.price}",
  //                       style: TextStyle(
  //                         color: Colors.red,
  //                         fontSize: 16,
  //                       ),
  //                     ),
  //                   ),
  //                   Align(
  //                     alignment: Alignment.centerRight,
  //                     child: Text(
  //                       '¥${e.oldPrice}',
  //                       style: TextStyle(
  //                         color: Colors.black54,
  //                         fontSize: 14,
  //                         decoration: TextDecoration.lineThrough,
  //                       ),
  //                     ),
  //                   )
  //                 ]),
  //               ),
  //             ],
  //           ),
  //         );
  //       }).toList(),
  //       runSpacing: 10,
  //       spacing: 10,
  //     ),
  //   );
  // }

  //热门推荐
  Widget _hotProductItemWidget() {
    var itemWidth = (ScreenUtil().screenWidth - 30) * 0.5;

    return Container(
      padding: EdgeInsets.all(10),
      width: itemWidth,
      decoration: BoxDecoration(
          border:
              Border.all(color: Color.fromRGBO(233, 233, 233, 0.9), width: 1)),
      child: Column(
        children: [
          Container(
            width: double.infinity,
            child: AspectRatio(
                aspectRatio: 1 / 1,
                child: Image.network(
                  "https://www.itying.com/images/flutter/list1.jpg",
                  fit: BoxFit.cover,
                )),
          ),
          Padding(
            padding: EdgeInsets.only(top: 20.h),
            child: Text(
              "2019夏季新款气质高贵洋气阔太太有女人味中长款宽松大码",
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(color: Colors.black54),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 20.h),
            child: Stack(children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "¥188.0",
                  style: TextStyle(
                    color: Colors.red,
                    fontSize: 16,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Text(
                  "¥198.0",
                  style: TextStyle(
                    color: Colors.black54,
                    fontSize: 14,
                    decoration: TextDecoration.lineThrough,
                  ),
                ),
              )
            ]),
          ),
        ],
      ),
    );
  }

  //猜你喜欢
  Widget _likeProductListWidget() {
    return Container(
      height: 228.h,
      padding: EdgeInsets.all(20.w),
      // decoration: BoxDecoration(
      //   color: Colors.blue,
      // ),
      child: ListView.builder(
        itemBuilder: ((context, index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: 140.w,
                height: 140.h,
                margin: EdgeInsets.only(right: 20.w),
                child: Image.network(
                  "https://www.itying.com/images/flutter/hot${index + 1}.jpg",
                  fit: BoxFit.cover,
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: 10.h,
                ),
                height: 44.h,
                width: 140.w,
                child: Text(
                  "第${index}条",
                  style: TextStyle(),
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                ),
                decoration: BoxDecoration(
                    // color: Colors.green,
                    ),
              )
            ],
          );
        }),
        scrollDirection: Axis.horizontal,
        itemCount: 10,
      ),
    );

//加载网络数据
    // return Container(
    //   height: 228.h,
    //   padding: EdgeInsets.all(20.w),
    //   // decoration: BoxDecoration(
    //   //   color: Colors.blue,
    //   // ),
    //   child: ListView.builder(
    //     itemBuilder: ((context, index) {
    //       // 图片处理
    //       String sPic = _hotProuctList[index].sPic;
    //       sPic = Config.domain + sPic.replaceAll('\\', '/');

    //       return Column(
    //         crossAxisAlignment: CrossAxisAlignment.start,
    //         children: [
    //           Container(
    //             width: 140.w,
    //             height: 140.h,
    //             margin: EdgeInsets.only(right: 20.w),
    //             child: Image.network(
    //               sPic,
    //               fit: BoxFit.cover,
    //             ),
    //           ),
    //           Container(
    //             padding: EdgeInsets.only(
    //               top: 10.h,
    //             ),
    //             height: 44.h,
    //             width: 140.w,
    //             child: Text(
    //               "¥${_hotProuctList[index].price}",
    //               style: TextStyle(),
    //               overflow: TextOverflow.ellipsis,
    //               textAlign: TextAlign.center,
    //             ),
    //             decoration: BoxDecoration(
    //                 // color: Colors.green,
    //                 ),
    //           )
    //         ],
    //       );
    //     }),
    //     scrollDirection: Axis.horizontal,
    //     itemCount: _hotProuctList.length,
    //   ),
    // );
  }

  // 轮播图
  Widget _swiperWidget() {
    List<Map> imgList = [
      {"url": "https://www.itying.com/images/flutter/slide01.jpg"},
      {"url": "https://www.itying.com/images/flutter/slide02.jpg"},
      {"url": "https://www.itying.com/images/flutter/slide03.jpg"}
    ];
    return Container(
      child: AspectRatio(
        aspectRatio: 2 / 1,
        child: Swiper(
          itemCount: imgList.length,
          itemBuilder: (context, index) {
            return Image.network(
              imgList[index]["url"],
              fit: BoxFit.fill,
            );
          },
          pagination: SwiperPagination(),
          autoplay: true,
          onTap: (index) {
            print('${index}');
          },
        ),
      ),
    );

//网络请求加载,接口问题,代码注释掉!!!
    // if (_focusData.length > 0) {
    //   return Container(
    //     child: AspectRatio(
    //       aspectRatio: 2 / 1,
    //       child: Swiper(
    //         itemCount: _focusData.length,
    //         itemBuilder: (context, index) {
    //           String pic = _focusData[index].pic;

    //           return Image.network(
    //             'http://jd.itying.com/${pic.replaceAll('\\', '/')}',
    //             fit: BoxFit.fill,
    //           );
    //         },
    //         pagination: SwiperPagination(),
    //         autoplay: true,
    //         onTap: (index) {
    //           print('${index}');
    //         },
    //       ),
    //     ),
    //   );
    // } else {
    //   return Text('加载中...');
    // }
  }

  // 猜你喜欢title
  Widget _titleWidget(title) {
    return Container(
      height: 32.0.w,
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.only(left: 20.w),
      padding: EdgeInsets.only(left: 10.w),
      decoration: BoxDecoration(
          // color: Colors.yellow,
          border: Border(left: BorderSide(color: Colors.red, width: 10.w))),
      child: Text(
        title,
        style: TextStyle(
            color: Colors.black54,
            // backgroundColor: Colors.green,
            fontSize: 22.sp),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
