import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/config/Config.dart';
import '../../model/CateModel.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CategoryPage extends StatefulWidget {
  const CategoryPage({Key? key}) : super(key: key);

  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage>
    with AutomaticKeepAliveClientMixin {
  int _selectIndex = 0;
  List _leftCateList = [];
  List _rightCateList = [];

  @override
  void initState() {
    super.initState();
    _getLeftCateData();
  }

  //左侧分类
  _getLeftCateData() async {
    var api = '${Config.domain}api/pcate';
    var result = await Dio().get(api);
    var leftCateList = CateModel.fromJson(result.data);
    setState(() {
      _leftCateList = leftCateList.result!;
    });
    _getRightCateData(leftCateList.result![0].sId);
  }

  //左侧分类
  _getRightCateData(pid) async {
    var api = '${Config.domain}api/pcate?pic=${pid}';
    var result = await Dio().get(api);
    var rightCateList = CateModel.fromJson(result.data);

    setState(() {
      _rightCateList = rightCateList.result!;
    });
  }

  @override
  Widget build(BuildContext context) {
    //左侧宽度
    var leftItemWidth = ScreenUtil().screenWidth / 4;
    //右侧每一项宽度
    var rightItemWidth =
        (ScreenUtil().screenWidth - leftItemWidth - 20 - 20) / 3;
//获取计算后的宽度
    var rightItemHeight = rightItemWidth + 28;

    return Row(
      children: [
        _leftCateWidget(leftItemWidth),
        _rightCateWidget(rightItemWidth, rightItemHeight)
      ],
    );
  }

//左Widget
  Widget _leftCateWidget(leftItemWidth) {
    //为了展示界面
    if (_leftCateList.length == 0) {
      return Container(
        width: leftItemWidth,
        height: double.infinity,
        child: ListView.builder(
          itemBuilder: ((context, index) {
            return Column(
              children: [
                InkWell(
                  onTap: () {
                    setState(() {
                      _selectIndex = index;
                      // _getRightCateData(this._leftCateList[index].sId);
                    });
                  },
                  child: Container(
                    width: double.infinity,
                    height: 84.h,
                    // child: Text("${this._leftCateList[index].title}",textAlign: TextAlign.center),
                    child: Center(
                      child: Text(
                        '第${index}条',
                        textAlign: TextAlign.center,
                      ),
                    ),
                    color: _selectIndex == index
                        ? Color.fromRGBO(240, 246, 246, 0.9)
                        : Colors.white,
                  ),
                ),
                Divider(
                  height: 1,
                ),
              ],
            );
          }),
          itemCount: 30,
          // itemCount: _leftCateList.length,
        ),
      );
    } else {
      return Container(
        width: leftItemWidth,
        height: double.infinity,
      );
    }
  }

  // 右widget
  Widget _rightCateWidget(rightItemWidth, rightItemHeight) {
    //为了展示界面
    if (_rightCateList.length == 0) {
      return Expanded(
          flex: 1,
          child: Container(
            margin: EdgeInsets.all(10),
            height: double.infinity,
            // color: Colors.red,
            child: GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  childAspectRatio: rightItemWidth / rightItemHeight,
                  mainAxisSpacing: 10,
                  crossAxisSpacing: 10,
                ),
                itemBuilder: ((context, index) {
                  //处理图片
                  // String pic = this._rightCateList[index].pic;
                  // pic = Config.domain + pic.replaceAll('\\', '/');

                  return InkWell(
                    onTap: () {
                      // Navigator.pushNamed(context, '/productList', arguments: {'cid':this._rightCateList[index].sId},);
                      Navigator.pushNamed(
                        context,
                        '/productList',
                        arguments: {'cid': '测试参数'},
                      );
                    },
                    child: Container(
                        child: Column(
                      children: [
                        AspectRatio(
                          aspectRatio: 1 / 1,
                          // child: Image.network("${pic}",fit: BoxFit.cover),
                          child: Image.network(
                              'https://www.itying.com/images/flutter/list8.jpg'),
                        ),
                        Container(
                          height: 28,
                          // child: Text("${this._rightCateList[index].title}"),
                          padding: EdgeInsets.only(top: 5),
                          child: Text(
                            '女装',
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    )),
                  );
                })),
          ));
    } else {
      return Expanded(
          flex: 1,
          child: Container(
            child: Text('加载中...'),
            height: double.infinity,
            color: Color.fromRGBO(240, 246, 246, 0.9),
            padding: EdgeInsets.all(10),
          ));
    }
  }

  @override
  bool get wantKeepAlive => true;
}
