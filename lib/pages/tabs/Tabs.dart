import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'Home.dart';
import 'Category.dart';
import 'Cart.dart';
import 'User.dart';

class Tabs extends StatefulWidget {
  const Tabs({Key? key}) : super(key: key);

  @override
  _TabsState createState() => _TabsState();
}

class _TabsState extends State<Tabs> {
  int _currentIndex = 3;

  late PageController _pageController;

  final List<Widget> _pageList = [
    const HomePage(),
    const CategoryPage(),
    const CartPage(),
    const UserPage()
  ];

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: _currentIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _currentIndex != 3
          ? AppBar(
              leading: IconButton(
                onPressed: null,
                icon: Icon(Icons.center_focus_weak),
              ),
              title: InkWell(
                child: Container(
                  height: 68.h,
                  padding: EdgeInsets.only(left: 10),
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(233, 233, 233, 0.8),
                    borderRadius: BorderRadius.circular(34.h),
                  ),
                  child: Row(
                    children: [
                      Icon(Icons.search),
                      Text(
                        '笔记本电脑',
                        style: TextStyle(fontSize: 28.sp),
                      ),
                    ],
                  ),
                ),
                onTap: (() {
                  Navigator.pushNamed(context, '/search');
                }),
              ),
              actions: [
                IconButton(
                  onPressed: null,
                  icon: Icon(
                    Icons.message,
                    size: 28,
                    color: Colors.black87,
                  ),
                ),
              ],
            )
          : AppBar(
              title: const Text('个人中心'),
            ),
      body: PageView(
        controller: _pageController,
        children: _pageList,
        onPageChanged: (value) {
          _currentIndex = value;
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: '首页'),
          BottomNavigationBarItem(icon: Icon(Icons.category), label: '分类'),
          BottomNavigationBarItem(
              icon: Icon(Icons.shopping_cart), label: '购物车'),
          BottomNavigationBarItem(icon: Icon(Icons.people), label: '我的')
        ],
        currentIndex: _currentIndex,
        fixedColor: Colors.blue,
        // selectedItemColor: Colors.yellow,
        // unselectedItemColor: Colors.red,
        onTap: (index) {
          setState(() {
            _currentIndex = index;
            _pageController.jumpToPage(_currentIndex);
          });
        },
        type: BottomNavigationBarType.fixed,
      ),
    );
  }
}
