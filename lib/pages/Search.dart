import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Container(
            padding: EdgeInsets.only(bottom: 5),
            child: TextField(
              autofocus: true,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                      borderSide: BorderSide.none)),
            ),
            height: 68.h,
            decoration: BoxDecoration(
              color: Color.fromRGBO(233, 233, 233, 0.8),
              borderRadius: BorderRadius.circular(34.h),
            ),
          ),
          actions: [
            InkWell(
              child: Container(
                height: 68.h,
                width: 80.w,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('搜索'),
                  ],
                ),
              ),
              onTap: () {
                print('444');
              },
            )
          ],
        ),
        body: Container(
          height: double.infinity,
          color: Colors.red,
          width: 200,
          child: Row(
            children: [
              Text('搜索'),
            ],
          ),
        ));
  }
}
