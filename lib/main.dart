import 'package:flutter/material.dart';
import 'package:flutter_app/routers/router.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      builder: ((context, child) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          initialRoute: '/',
          onGenerateRoute: onGenerateRoute,
          theme: ThemeData(
            primaryColor: Colors.white,
          ),
        );
      }),
      designSize: const Size(750, 1334),
      minTextAdapt: true,
      splitScreenMode: true,
    );
  }
}
