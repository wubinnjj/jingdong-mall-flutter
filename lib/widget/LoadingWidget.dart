import 'package:flutter/material.dart';

class LoadingWidget extends StatelessWidget {
  const LoadingWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              child: CircularProgressIndicator(
                strokeWidth: 1.0,
              ),
              width: 16.0,
              height: 16.0,
              margin: EdgeInsets.only(right: 10.0),
            ),
            Text(
              '加载中...',
              style: TextStyle(fontSize: 16.0),
            )
          ],
        ),
      ),
    );
  }
}
